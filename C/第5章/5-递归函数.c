#include <stdio.h>
#include <stdlib.h>

int fib(int n) //求n的阶乘, fib(n) = fib(n-1) x n
{
	if( n<= 1) return 1;   //递归的出口条件
	return n * fib(n-1); 
}

int sum(int n)//求1+2+3+...+n的和
{
	if( n<=1 ) return 1;  //递归的出口条件
	return n + sum(n-1);
}

int main()
{
	int n;
	printf("请输入n: ");
	scanf("%d", &n);
	
	printf("%d! = %d \n", n, fib(n));
	printf("1+2+..+%d = %d \n", n, sum(n));
	
	return 0;
}
