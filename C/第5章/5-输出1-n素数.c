#include <stdio.h>
#include <stdlib.h>



 //判断n是否为素数，返回0-不是，否则是素数
int isPrime(int n)
{
	//试因子，从2到 n/2
	int i;  //引用全局变量
	
	if( n==2 || n==3 ) return 1;
	
	for(i=2; i<=n/2; i++)
	{//i 是否 n的因子
		if( n%i == 0 ) return 0;  //不是素数
	}
	return 1;
}


int main()
{
	
	int n, i;  //局部变量, 当函数结束后，变量不存在
	printf("求1-n之间的素数, 请输入n: ");
	scanf("%d", &n);
	
	for(i=2;i<=n;i++ ){
		if( isPrime(i) ) printf("%d ", i);
	}
	
	return 0;
}


