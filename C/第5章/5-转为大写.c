#include <stdio.h>
#include <stdlib.h>
#include <string.h>

//将字符串 str中的小写字母变为大写字母
void ToUpper(char* str); //函数的声明

int main()
{
	char str[80];
	printf("请输入字符串：");
	gets(str);  //得到用户的字符串
	
	//把小写变为大写
	ToUpper(str);
	
	printf("转换为大写字母的字符串: %s \n", str);
	
	return 0;
}

//将字符串 str中的小写字母变为大写字母
void ToUpper(char str[])
{
	int i, len;
	len = strlen(str);
	for(i=0;i<len;i++) //对每个字符进行处理
	{
		char ch = str[i];  // 如果ch是小写字母，就将它变为大写字母
		if( (ch >= 'a') && (ch <='z') )  //小写字母
		{
			ch =  ch - 'a'  + 'A'; //转为大写字母
			str[i] = ch; //将字符串中的小写字母变为大写字母
		}
	}
}

