#include <stdio.h>
#include <stdlib.h>

static int price = 168168;  //static 定义的全局变量，只在本文件内可见

void sayHello(); //声明

int main()
{
	extern int value;  //引用某个全局变量
	//extern int price;  //
	
	int i;
	for(i=0;i<10;i++)  sayHello();
	
	return 0;
}

void sayHello()
{
	static int count = 100;  //定义, 函数内的static变量，生命周期和程序一样，但是仅仅函数内可见。
	
	count ++;
	printf("Hello, C! count = %d \n", count);
}

//全局变量定义
int value = 123456;



