#include <stdio.h>
#include <stdlib.h>

int main()
{
	int n ;
	int g,s,b;
	
	printf("请输入3位的整数: ");
	scanf("%d", &n);
	
	g = n %10;
	s = (n/10)%10;
	b = (n/100)%10;
	
	printf("个位数=%d, 十位数=%d, 百位数=%d \n", g,s,b);
	
	return 0;
}
