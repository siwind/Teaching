#include <stdio.h>
#include <stdlib.h>

int main()
{
	int n;  //输入的4位数
	int g,s,b,q;  //个位,十位,百位, 千位
	
	printf("请输入4位整数: ");
	scanf("%d", &n);
	
	g = (n)%10;
	s = (n/10)%10;
	b = (n/100)%10;
	q = (n/1000)%10;
	
	g = (g+9)%10;
	s = (s+9)%10;
	b = (b+9)%10;
	q = (q+9)%10;
	
	n = b + q*10 + g*100 + s*1000;  //新数
	
	printf("加密后的数是:  %d \n", n);
	
	return 0;
}
