#include <stdio.h>
#include <stdlib.h>

int main()
{
	int n, sum, i, k;
	int ge;  //个位数
	
	printf("求1-n之间的所有水仙花数，请输入n: ");
	scanf("%d", &n);
	
	for(i=1;i<=n; i++) // 顺序判断1,2,3,..., n , 看看是否水仙花数
	{
		sum = 0;   //记录每个数字的立方和的 和数
		k = i;     //判断k是否水仙花数
		while(k > 0){
			ge = k%10;
			sum = sum + ge*ge*ge;  //累加
			k = k/10;   //缩小10倍
		}
		//判断 i == sum , 是水仙花数
		if( i == sum ){
			printf("%d ",i);
		}
	}
	
	return 0;
}
