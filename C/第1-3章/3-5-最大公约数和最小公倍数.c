#include <stdio.h>
#include <stdlib.h>

int gcd(int a,int b)//最小公倍数, a,b正整数
{//(a,b) =(b,a%b)=...=(d,0) = d
	int t;
	if(a<b){//保证 a>b
		t = a;
		a = b;
		b = t;
	}
	while(b>0){
		t = b;
		b = a%b;
		a = t;
	}
	return a;
}

int main()
{
	int a,b,d, m; //a,b,d-最大公约数,m-最小公倍数
	printf("请输入正整数a,b: ");
	scanf("%d%d", &a,&b);
	
	d = gcd(a,b);
	m = a*b/d;
	
	printf("最大公倍数=%d, 最小公约数=%d \n",m,d);
	return 0;
}
