#include <stdio.h>
#include <stdlib.h>

float power(float base,  int exp)
{
	float sum = 1.0;
	for(int i=0; i<exp;i++)
		sum = sum * base;
	
	return sum;
}

int main()
{
	float money, rate, interest ;// 本金, 利率, 利息
	float total;  //总收入
	int year;  //存期
	
	printf("请输入 本金, 存期, 利率: ");
	
	scanf("%f%d%f", &money, &year, &rate);
	
	total = money *power(1+rate, year);
	interest = total - money;
	
	printf("利息是: %.2f \n", interest);
	
	return 0;
}
