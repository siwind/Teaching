#include <stdio.h>
#include <stdlib.h>

int main()
{
	int h,m;  //小时和分钟
	float d1, d2,diff; //角度1，角度2,差值
	
	printf("请输入时间(hh:mm) ");
	scanf("%d:%d", &h,&m);
	
	//调整小时, 特殊情况 m=60, 1:60 = 2:00
	h = (h + m/60) % 12;  //小时处理为12小时制
	m = m % 60;  //调整分钟
	
	d1 = m*6;
	d2 = h*30 + m/2.0;
	
	diff = (d1>d2)?d1-d2:d2-d1;  //三目运算符
	
	diff = (diff>180)? 360-diff:diff;  //调整为0-180度之间
	
	printf("时针和分针的夹角是: %.1f \n", diff);
		
	return 0;
}
