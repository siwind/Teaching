#include <stdio.h>
#include <stdlib.h>

int main()
{
	int num, alpha, space,cr,other;//数字，字母，空格,回车,其它的数目
	char ch; //输入的字符
	
	num = alpha = space = cr = other = 0;//初始化
	printf("请输入一行字符串: ");
	fflush(stdin);
	
	while( (ch=getchar()) != '\n'){ //行结束的判断
		if( ch == ' ') space++;
		else if(ch == '\r') cr++;
		else if(ch>='0' && ch<='9') num++;
		else if(ch>='a' && ch<='z') alpha ++;
		else if(ch>='A' && ch<='Z') alpha ++;
		else other++;
	}
	
	printf("数字=%d, 字母=%d, 空格=%d,回车=%d,其它=%d \n",num,alpha,space,cr,other);
	
	return 0;
}
