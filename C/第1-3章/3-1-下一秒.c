#include <stdio.h>
#include <stdlib.h>

int main()
{
	int h, m, s;  //小时，分钟，秒
	int sum ;  //记录进位的情况
	
	printf("请输入时间(hh:mm:ss): ");
	scanf("%d:%d:%d", &h,&m,&s);
	
	sum = s + 1;
	s = sum % 60;  //新的秒数
	
	sum = m + sum/60; //新的和数
	m = sum %60;  //新的分钟，
	
	h = (h + sum/60) %24; // 24小时制
	
	printf("下一秒是  %02d:%02d:%02d \n", h,m,s);
	
	return 0;
}
