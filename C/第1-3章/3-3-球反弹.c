#include <stdio.h>
#include <stdlib.h>

float getHeight(int m) //求第m次反弹的高度
{
	float h = 1.0;
	int i;
	for(i=0;i<m;i++)
	{
		h = h/4;
	}
	return h;
}

float getTotal(int m) //求第m次次落地时的全部路程
{
	float h=1,len = 0;
	int i;
	
	len = 1;  //第一次落地
	
	for(i=2;i<=m;i++)
	{	
		h = h/4;
		len = len + h + h;
	}
	return len;
}


int main()
{
	int n, m;  //n-高度, m-次数
	float len, h; //总路程和反弹高度
	
	printf("请输入高度和次数:");
	scanf("%d%d", &n,&m);
	
	len = n* getTotal(m);
	h = n* getHeight(m);
	
	printf("总路程=%.3f, 反弹高度=%.3f\n", len, h);
	return 0;
}

