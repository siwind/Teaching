#include <stdio.h>
#include <stdlib.h>

int getLen(int n) //求n的基数， 123-->1000
{
	int s = 1;
	while(n>0){
		n = n/10;
		s = s*10;
	}
	return s;
}

int main()
{
	int i,n, base; //
	
	printf("求1-n之间的所有同构数,请输入n: ");
	scanf("%d", &n);
	
	for(i=0;i<=n;i++){
		base = getLen(i);

		if( i == (i*i)%base) printf("%d ",i); 
	}
	printf("\n");	
	return 0;
}
