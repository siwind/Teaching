#include <stdio.h>
#include <stdlib.h>

//得到一个正整数的10的基数，3->10,  12-->100, 235-->1000,  12345->100000,....
int GetBase(int n)
{
	int s = 1;
	while(n>0){
		s = s * 10;
		n = n/10;   //n缩小10倍
	}
	return s; //返回对应的基数
}

int main()
{
	int i, n;  //1,2,3,...  ,n  之间的所有自收数
	int sum;   
	printf("求1,2,3,..., n之间的所有自收数，请输入n: ");
	scanf("%d", &n);
	
	for(i = 1; i<=n; i++){
		sum = (i*i) % GetBase(i);  //平方再求摸
		
		if( i == sum )
			printf("%d=%d*%d, ", i*i,i,i);
	}
	
	return 0;
}

