#include <stdio.h>
#include <stdlib.h>

int main()
{
	int i,n;  //第n天
	int rich=10;
	float poor=1, total=1; //富翁和穷人拿到的钱数
	printf("请输入天数: ");
	scanf("%d", &n);
	
	for(i=2;i<=n;i++){//从第2天开始
		rich += 10;  //每天加10万
		poor = poor*2;  //每天翻倍
		total += poor; //每天的钱要加起来
	}
	
	printf("富翁的收获=%d 万元，穷人的收获=%f 万元 \n",rich, total/1000000.0);
	
	return 0;
}
