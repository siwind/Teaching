#include <stdio.h>
#include <stdlib.h>


//判断n是否素数，是素数返回真-1，不是素数返回0
int IsPrime(int n)
{
	int i, result = 1;  //result的初始值是1-真
	for(i=2;i<n/2;i++)
	{
		if( n/i == 0 )  //n含有因子i, 因此n不是素数
		{
			result = 0;
			break;
		}
	}
	//
	return result;  //返回结果
}
int main()
{
	int i, n;    //6,8,10,12,...., n,  验证哥德巴赫猜想
	int a,b;     //a,b 都是奇数
	
	printf("对6,8,10,..,n 验证哥德巴赫猜想，请输入n: ");
	scanf("%d", &n);
	
	for(i=6;i<=n;i+=2)
	{//验证i = 6，8，10，....是否满足猜想， i = a + b ,  其中的a,b都是素数
		for(a = 3; a<=i/2; a+=2)
		{
			b = i - a;
			if( IsPrime(a) && IsPrime(b) )  //判断 a 和 b是否素数  
			{
				printf("%d = %d + %d, ", i,a,b);
				break;  //不要重复对i进行验证
			}
		}
	}
	
	return 0;
}
