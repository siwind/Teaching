#include <stdio.h>
#include <stdlib.h>

int main()
{
	float c ; //摄氏温度
	float f;  //华氏温度
	
	printf("请输入摄氏温度值： ");
	scanf("%f", &c);
	
	f = c*9/5 + 32;
	
	printf("华氏温度是： %.1f \n", f);
	
	return 0;
}
