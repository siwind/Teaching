#include <stdio.h>
#define PI   3.1415926535 
//x belongs [0,PI/2)
//sin(x) = x - x^3/3! + x^5/5! +.. +(-1)^n x^(2n-1)/(2n-1)! + ...
//sin(x+2*PI) = sin(x);  sin(x+PI) = -sin(x) ; sin(PI-x) = sin(x)
double sinx(double x, int n){
	double sum = 0, pow;
	int i,j,sign = 1,flag=1;
	//x<0?
	x = (x<0)? (sign =-sign, -x) :x; //x>=0
	//x>=2*PI
	while(x>=2*PI)  x -= 2*PI; //
	if( x >= PI){
		x = x - PI; //
		sign = -sign;
	}//x < PI
	x = (x>=PI/2)?(PI-x):x;  // x <PI/2
	
	if( x<= 1e-6 ) return 0; //
	
	for(i=1;i<=n;i++){
		pow = 1;
		for(j=1;j<=2*i-1;j++) {//  x^(2i-1)/(2i-1)!
			pow = pow*(x/j); //
		}
		sum = sum + flag*pow;
		flag = -flag;  //
	}
	return sign * sum; //
}
int main()
{
	double x;
	int n;
	printf("计算sinx,请输入x和n： ");
	scanf("%lf%d", &x, &n);
	printf("sin(%.2lf)=%.12lf \n", x, sinx(x,n));
	return 0;
}
