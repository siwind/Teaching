#include <stdio.h>
#include <stdlib.h>

#include <math.h>  // sqrt(float/double),  求平方根

int main()
{
	int a,b,c;  //三边长，整数
	float area,s; //面积, 半周长
	
	printf("请输入三边长：");
	scanf("%d%d%d", &a,&b,&c);
	
	if( (a + b <=c) || (b+c<=a) || (c+a<=b) )
	{
		printf("a,b,c三边不满足构成三角形的条件! \n");
		return 0;
	}
	
	s = (a+b+c)/2.0; //半周长
	
	area = sqrt(s*(s-a)*(s-b)*(s-c));  //面积
	
	printf("面积是： %.3f \n", area);

	return 0;
}
