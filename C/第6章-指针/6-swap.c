#include <stdio.h>
#include <stdlib.h>

void swap1(int a, int b){//交换变量的值
	int t = a;
	a = b;
	b = t;
}

void swap2(int* pa, int* pb){//交换变量的值
	int t = *pa;
	*pa = *pb;
	*pb = t;
}

void funaddr(){
	int* p = (int*) swap1;  //函数名就是函数的开始地址
	printf("Address of function swap1 =  0x%0x \n", (unsigned int)p); //
}

int main()
{
	int x=10,y=20;
	swap1(x,y);
	
	funaddr();
	
	return 0;
}
