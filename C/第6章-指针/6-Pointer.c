#include <stdio.h>
#include <stdlib.h>

void arrptr(){
	int a[] = {1,2,3,4,5,6};//
	int i;
	int* p;
	
	
	for(i=0;i<6;i++){
		printf("%d ", *(a+i) );  //a[i]
	}
	printf("\n");
	p = &a[0];//
	for(i=0;i<6;i++){
		printf("%d ", p[i]); //*(p+i)
	}
}

int main()
{
	arrptr();
	return 0;
}
