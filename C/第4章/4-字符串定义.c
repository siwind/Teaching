#include <stdio.h>
#include <stdlib.h>

int main()
{
	char a[4] = {'A','B','C','D'};
	char b[4] = {'1','2','3','\0'};
	char c[4] = {'x','y','z'};
	
	printf("a=%s \n", a);
	printf("b=%s \n", b);
	printf("c=%s \n", c);
	
	return 0;
}
