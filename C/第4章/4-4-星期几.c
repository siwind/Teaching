﻿#include <stdio.h>
#include <stdlib.h>

//四年一闰，百年不闰，四百年再闰
int isLeap(int y){//如果是闰年，返回1; 否则返回0
	if((y%4 == 0 && y%100 !=0) || (y%400 == 0)) return 1;
	else return 0;
}
//y年M月d日： 距离1年1月1日的天数
//例如： 1-1-1--> 1天；
//       1-1-20 --->20天
int SumDate(int y, int m, int d){
	int sum = 0, i;
	int YEAR[]={365,366};
	int MONTH[]={31,28,31,30,31,30,31,31,30,31,30,31};
	
	for(i=1;i<y;i++){//计算1年, 2年，.., (y-1)年的天数
		sum += YEAR[isLeap(i)];  //第i年是否闰年，表格驱动
	}
	//计算第y年的天数
	//计算从1月,2月,..., (m-1)月的整月天数
	for(i=1;i<m;i++){
		sum +=  MONTH[i-1];//   
		//if(m>2) sum += isLeap(y); //闰年的2月要多一天
	}
	if(m>2) sum += isLeap(y); //闰年的2月要多一天
	//第m月有d天
	sum += d;
	
	return sum %7;  //一周有7天
}

int main()
{
	int y,m,d;
	int s, r;
	const char* WEEK[] = {"星期天","星期一","星期二","星期三","星期四","星期五","星期六"};
	
	printf("请输入日期(YYYY-MM-DD) : ");
	scanf("%d-%d-%d", &y,&m,&d);  //得到日期
	
	s = SumDate(y,m,d);  //
	r = SumDate(1980,1,1); //1980-1-1 的天数
	
	//调整
	s = (s - r + 2 + 7)%7;  // s->0-周日, 1-周一,.., 6-周六
	
	//输出
	printf("%d年%d月%d日是： %s \n", y,m,d, WEEK[s]);
	
	return 0;
}

