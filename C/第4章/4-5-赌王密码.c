#include <stdio.h>
#include <stdlib.h>
//6x6的一个字符矩阵
char msg[6][7]; //存储输入的数据, 存'\0'
int IndexA(char* str){//判断字符串str中字母'A'的位置，并返回位置
	//str="13bA6H" -->4, "Anh68y" -->1
	int i;
	for(i=0;i<6;i++){
		if(str[i] == 'A') return (i+1); //Caution!!
	}
	return 0; //未找到'A'，返回0
}

int main()
{
	int i,sum=0;
	printf("请输入6行字符串，每行6个字符: \n");
	for(i=0;i<6;i++){
		scanf("%s", msg[i]);
	}
	for(i=0;i<6;i++){
		//判断msg[i]行，看看‘a'在哪个位置
		sum = sum*10 + IndexA(msg[i]);
	}
	
	printf("赌王的密码是： %d \n", sum);
	
	return 0;
}
