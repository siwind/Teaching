#include <stdio.h>
#include <stdlib.h>

int DrawLine(int index, int p){//index-序号, p-百分比
	int i;
	printf("%2d ( %2d%% ) ", index, p);
	for(i=0;i<p;i++) printf("#");
	
	//换行
	printf("\n");
}

int main()
{
	float m[12], sum = 0;
	int i;
	printf("请输入12个月的营业额: \n");
	
	for(i=0;i<12;i++){
		scanf("%f", &m[i]); //得到12月的营业额
		sum += m[i];  //求和
	}
	//每个月的百分比:   m[i]/sum x 100 -->取整
	for(i=0;i<12;i++){
		DrawLine(i+1, (int)(m[i]*100/sum) );
	}
	return 0;
}




