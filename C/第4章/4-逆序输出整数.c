#include <stdio.h>
#include <stdlib.h>

int main()
{
	unsigned int n ;  //用户输入的整数
	unsigned int m = 0;  //逆序的整数
	
	printf("请输入正整数: ");
	scanf("%u", &n);
	
	while(n>0)
	{
		m = m*10 + n%10;   //m放大10倍
		n = n/10;    //n缩小10倍
	}
	
	printf("逆序后的整数是: %u \n", m);
	return 0;
}
