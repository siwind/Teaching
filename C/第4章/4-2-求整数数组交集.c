#include <stdio.h>
#include <stdlib.h>

int main()
{
	int a[20],b[20],c[20]; //求数组a和数组b的交集, 结果是数组c
	int m,n,p; //m-数组a的元素个数,n-数组b的元素个数,p-数组c的元素个数
	int i,j,k;
	
	printf("请输入数组A的元素个数：");
	scanf("%d", &m);
	for(i=0;i<m;i++)  scanf("%d", &a[i]);
	
	printf("请输入数组B的元素个数：");
	scanf("%d", &n);
	for(i=0;i<n;i++)  scanf("%d", &b[i]);
	
	//求交集
	p = 0; //数组C开始是没有元素的
	for(i=0;i<m;i++){//取出数组A的第i个元素
		for(j=0;j<n;j++){
			if( a[i] == b[j] ){//得到了交集中的一个元素
				for(k=0;k<p;k++){
					if( c[k] == a[i] ) break;
				}//
				if( k>=p ) {//没有重复
					c[p] = a[i]; //
					p++; //p是下标，同时代表了C的数目
				}
				break;
			}
		}
	}
	//输出交集
	printf("数组A和B的交集是：{");
	for(i=0;i<p;i++){
		printf("%d ", c[i]);
	}
	printf("}\n");
	
	return 0;
}
