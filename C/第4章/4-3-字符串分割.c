#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main()
{
	char str[80], tok[80], sp[80]; //字符串和分割符号
	int i, j,len;
	char ch;
	
	printf("请依次输入2个字符串: ");
	gets(str);
	gets(tok);
		
	//处理, 分割
	len = strlen(str);
	
	for(i=0;i<strlen(tok);i++){//对tok取每个字符
		ch = tok[i];  //用ch字符去分割str
		for(j=0;j<len;j++){
			if( ch == str[j] )  str[j] = '\0';
		}
	}
	//输出
	j = 0;
	sp[0] = '\0'; //初始化
	for(i=0;i<len;i++){
		if( str[i] == '\0' ){ //输出split
			sp[j] = '\0';
			printf("%s \n",sp);
			
			j=0;
			sp[j] = '\0';
		}else{
			sp[j] = str[i];
			j++;
		}
	}
	sp[j] = '\0';
	printf("%s \n",sp);
	
	return 0;
}
