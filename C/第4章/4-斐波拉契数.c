#include <stdio.h>
#include <stdlib.h>


int fib(int n)  //求第 n 个斐波拉契数,  fib(0) = fib(1) = 1
{
	int i, a,b,c;
	
	if(n <=1){  //第0，1个fib数
		return 1;
	}
	
	a = b = 1; //初始化
	
	for(i = 2; i<=n; i++)
	{
		c = a + b;   //
		a = b;     //
		b = c;     //
	}
	
	return c;
}

int main()
{
	int i, v;
	
	printf("请输入欲计算的第几位斐波拉契数(f(0)=f(1)=1：");
	
	scanf("%d", &i);
	
	v = fib(i);
	
	printf("第 %d 位 斐波拉契数是: %d \n", i, v);
	
	return 0;
}
