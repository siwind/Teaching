#include <stdio.h>
#include <stdlib.h>

int main()
{
	int i,j,data,c,num;  //data-每个整数，num - 用户输入的整数的数目
	int val[100], count[100];//整数及出现次数	
	
	printf("请输入整数个数: ");
	scanf("%d", &num); 
	
	for(i=0;i<num;i++) {//边输入整数
		scanf("%d", &val[i]); //第i个数
	}
	
	//判断次数, 并且消除重复的元素
	for(i=0;i<num;i++){
		data = val[i]; //判断第i个元素的次数
		count[i] = 0;  //初始次数是0
		for(j=0;j<num;j++){
			if(data == val[j] ) {
				count[i] ++;
			}
			
		}
	}
	
	//取出现最多的元素和次数  (消除重复的情况), c记录了出现最多的次数
	c = count[0];
	for(i=1;i<num;i++) {
		if( c < count[i] )  c = count[i]; //找出现次数最多
	}
	
	//输出出现最多的元素和次数
	for(i=0;i<num;i++){  //不要输出重复的元素
		if( count[i] == c ) {//得到次数最多的元素
			
			for(j=0;j<i;j++){//0-i之前是否输出了 count[i]
				if( val[j] == val[i] ) break;
			}
			if(j>=i) printf("整数=%d, 次数=%d \n", val[i], count[i]);   //之前没有输出过
		}
	}
	
	return 0;
}
