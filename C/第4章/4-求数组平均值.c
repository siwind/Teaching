#include <stdio.h>
#include <stdlib.h>

float average(float a[10])
{
	float sum = 0;
	for(int i=0;i<10;i++)
	{
		sum +=  a[i];
	}
	//和
	sum = sum / 10;
	return sum;  
}

int main()
{
	float score[10], avg;
	int i;
	
	printf("请输入10个学生的成绩: ");
	
	for(i=0;i<10;i++)  scanf("%f", &score[i]);
	
	avg = average(score);
	
	printf("平均成绩是: %f \n",avg);
	return 0;
}

